﻿namespace AKUNTING
{
    partial class industriclass
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel5 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtindustriid = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtname = new System.Windows.Forms.TextBox();
            this.btnsimpanindustri = new System.Windows.Forms.Button();
            this.btnsimpanbisns = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtbisnisid = new System.Windows.Forms.TextBox();
            this.cbindustriname = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtidindustri = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtnamebisnis = new System.Windows.Forms.TextBox();
            this.dgbisnis = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgbisnis)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.LightSeaGreen;
            this.panel5.Controls.Add(this.pictureBox1);
            this.panel5.Controls.Add(this.label8);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(962, 81);
            this.panel5.TabIndex = 31;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::AKUNTING.Properties.Resources.ACCOUNTANT_512;
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(95, 75);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(104, 24);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(182, 24);
            this.label8.TabIndex = 0;
            this.label8.Text = "Industri Classification";
            // 
            // txtindustriid
            // 
            this.txtindustriid.Location = new System.Drawing.Point(86, 41);
            this.txtindustriid.Name = "txtindustriid";
            this.txtindustriid.Size = new System.Drawing.Size(148, 20);
            this.txtindustriid.TabIndex = 33;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 33;
            this.label1.Text = "Industri ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 34;
            this.label2.Text = "Name";
            // 
            // txtname
            // 
            this.txtname.Location = new System.Drawing.Point(86, 67);
            this.txtname.Name = "txtname";
            this.txtname.Size = new System.Drawing.Size(148, 20);
            this.txtname.TabIndex = 35;
            // 
            // btnsimpanindustri
            // 
            this.btnsimpanindustri.Location = new System.Drawing.Point(86, 93);
            this.btnsimpanindustri.Name = "btnsimpanindustri";
            this.btnsimpanindustri.Size = new System.Drawing.Size(148, 23);
            this.btnsimpanindustri.TabIndex = 33;
            this.btnsimpanindustri.Text = "Simpan Industri";
            this.btnsimpanindustri.UseVisualStyleBackColor = true;
            this.btnsimpanindustri.Click += new System.EventHandler(this.btnsimpanindustri_Click);
            // 
            // btnsimpanbisns
            // 
            this.btnsimpanbisns.Location = new System.Drawing.Point(89, 131);
            this.btnsimpanbisns.Name = "btnsimpanbisns";
            this.btnsimpanbisns.Size = new System.Drawing.Size(121, 23);
            this.btnsimpanbisns.TabIndex = 33;
            this.btnsimpanbisns.Text = "Simpan Bisnis";
            this.btnsimpanbisns.UseVisualStyleBackColor = true;
            this.btnsimpanbisns.Click += new System.EventHandler(this.btnsimpanbisns_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 13);
            this.label3.TabIndex = 34;
            this.label3.Text = "Industri Name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 33;
            this.label4.Text = "Business ID";
            // 
            // txtbisnisid
            // 
            this.txtbisnisid.Location = new System.Drawing.Point(89, 27);
            this.txtbisnisid.Name = "txtbisnisid";
            this.txtbisnisid.Size = new System.Drawing.Size(121, 20);
            this.txtbisnisid.TabIndex = 33;
            // 
            // cbindustriname
            // 
            this.cbindustriname.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbindustriname.FormattingEnabled = true;
            this.cbindustriname.Location = new System.Drawing.Point(89, 52);
            this.cbindustriname.Name = "cbindustriname";
            this.cbindustriname.Size = new System.Drawing.Size(121, 21);
            this.cbindustriname.TabIndex = 35;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 82);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 13);
            this.label5.TabIndex = 36;
            this.label5.Text = "Industry ID";
            // 
            // txtidindustri
            // 
            this.txtidindustri.Location = new System.Drawing.Point(89, 79);
            this.txtidindustri.Name = "txtidindustri";
            this.txtidindustri.ReadOnly = true;
            this.txtidindustri.Size = new System.Drawing.Size(121, 20);
            this.txtidindustri.TabIndex = 37;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 108);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 38;
            this.label6.Text = "Name";
            // 
            // txtnamebisnis
            // 
            this.txtnamebisnis.Location = new System.Drawing.Point(89, 105);
            this.txtnamebisnis.Name = "txtnamebisnis";
            this.txtnamebisnis.Size = new System.Drawing.Size(263, 20);
            this.txtnamebisnis.TabIndex = 39;
            // 
            // dgbisnis
            // 
            this.dgbisnis.AllowUserToAddRows = false;
            this.dgbisnis.AllowUserToDeleteRows = false;
            this.dgbisnis.BackgroundColor = System.Drawing.Color.White;
            this.dgbisnis.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgbisnis.Location = new System.Drawing.Point(417, 93);
            this.dgbisnis.Name = "dgbisnis";
            this.dgbisnis.ReadOnly = true;
            this.dgbisnis.Size = new System.Drawing.Size(533, 322);
            this.dgbisnis.TabIndex = 40;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.btnsimpanbisns);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.cbindustriname);
            this.groupBox1.Controls.Add(this.txtbisnisid);
            this.groupBox1.Controls.Add(this.txtnamebisnis);
            this.groupBox1.Controls.Add(this.txtidindustri);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Location = new System.Drawing.Point(19, 236);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(374, 179);
            this.groupBox1.TabIndex = 41;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Data Bisnis Class";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnsimpanindustri);
            this.groupBox2.Controls.Add(this.txtindustriid);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtname);
            this.groupBox2.Location = new System.Drawing.Point(19, 93);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(374, 137);
            this.groupBox2.TabIndex = 42;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Data Industri Class";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(216, 50);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(51, 23);
            this.button1.TabIndex = 40;
            this.button1.Text = "Cek";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // industriclass
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(962, 466);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.dgbisnis);
            this.Name = "industriclass";
            this.Text = "industriclass";
            this.Load += new System.EventHandler(this.industriclass_Load);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgbisnis)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnsimpanindustri;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtname;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtindustriid;
        private System.Windows.Forms.Button btnsimpanbisns;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtbisnisid;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtidindustri;
        private System.Windows.Forms.ComboBox cbindustriname;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtnamebisnis;
        private System.Windows.Forms.DataGridView dgbisnis;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button1;
    }
}